<?php
/**
 * Created by PhpStorm.
 * User: cosmin.croitoru
 * Date: 9/15/2019
 * Time: 8:25 PM
 */

use App\Internal\CacheHandler;

class CacheHandlerTest extends TestCase
{
    private $cacheHandler = null;

    public function setTestGetDataToRenderData()
    {
        return [
            [
                'values' => [
                    'items' => [
                        [
                            \App\Internal\JsonProcessor::JSON_IMAGE_KEY[0] => [
                                [
                                    'url' => 'test/url'
                                ]
                            ],
                            \App\Internal\JsonProcessor::JSON_IMAGE_KEY[1] => [
                                [
                                    'url' => 'test/url'
                                ]
                            ],
                            'lastUpdated' => '2019-01-01'
                        ]
                    ],
                    'keyExists' => true,
                    'cacheValue' => 'hashedvalueasimagecache'
                ], 'expected' => [
                    [
                        'cardImages' =>
                            [
                                [
                                    'url' => 'test/url',
                                    'cache' => 'hashedvalueasimagecache',
                                ],
                            ],
                        'keyArtImages' =>
                            [
                                [
                                    'url' => 'test/url',
                                    'cache' => 'hashedvalueasimagecache',
                                ],
                            ],
                        'lastUpdated' => '2019-01-01',
                    ],
                ]
            ], [
                'values' => [
                    'items' => [
                        [
                            \App\Internal\JsonProcessor::JSON_IMAGE_KEY[0] => [
                                [
                                    'url' => 'test/url'
                                ]
                            ],
                            \App\Internal\JsonProcessor::JSON_IMAGE_KEY[1] => [
                                [
                                    'url' => 'test/url'
                                ]
                            ],
                            'lastUpdated' => '2019-01-01'
                        ],
                    ],
                    'keyExists' => false,
                    'cacheValue' => 'tryfgfgdsgdsgssg'

                ], 'expected' => [
                    [
                        'cardImages' =>
                            [
                                [
                                    'url' => 'test/url',
                                ],
                            ],
                        'keyArtImages' =>
                            [
                                [
                                    'url' => 'test/url',
                                ],
                            ],
                        'lastUpdated' => '2019-01-01',
                    ],
                ]
            ]
        ];
    }

    /** @dataProvider setTestGetDataToRenderData */
    public function testGetDataToRender($values, $expected)
    {
        $redis = new \Illuminate\Support\Facades\Redis();
        app()->instance(\Illuminate\Support\Facades\Redis::class, $redis);

        $redis->shouldReceive('exists')
            ->with(Mockery::type('string'))
            ->andReturn($values['keyExists']);

        $redis->shouldReceive('get')
            ->with(Mockery::type('string'))
            ->andReturn($values['cacheValue']);
        $result = $this->getCacheHandler()->getDataToRender($values['items']);
        //die(var_export($result));
        $this->assertEquals($expected, $result);
    }

    protected function getCacheHandler()
    {
        if (null === $this->cacheHandler) {
            $this->cacheHandler = new CacheHandler();
        }

        return $this->cacheHandler;
    }
}
