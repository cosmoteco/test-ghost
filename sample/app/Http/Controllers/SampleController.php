<?php

namespace App\Http\Controllers;

use App\Email;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class SampleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
	{
	}

    /**
     * {
     * "emails": [
     *       {
     *           "email": "user@example.com"
     *           "client": "Test1"
     *        },
     *       {
     *           "email": "mike@hotmail.ninja"
     *           "client": "test2"
     *       }
     *   ]
     *  }
     * @param Request $request
     * @return json
     */
	public function create(Request $request) {
        try {
            $this->validate($request, [
                'emails' => 'required|array',
                'emails.*.email' => 'required|email',
                'emails.*.client' => 'required|string',
            ]);

            $data = $request->all();
            foreach ($data['emails'] as $email) {
                $emailAddress = filter_var($email['email'], FILTER_SANITIZE_EMAIL);
                $client = filter_var($email['client'], FILTER_SANITIZE_STRING);

                $insertedEmails[$email['client']][] = $emailAddress;
                DB::insert('insert into email (address,client_identifier) values (?, ?)',
                    [
                        $emailAddress,
                        $client
                    ]
                );
            }
        } catch (\Exception $ex) {
            return response()->json([
                'success' => false,
                'message' => 'Internal server error'
            ], 500);
        }

        return response()->json(
            [
                'success' => true,
                'message' => $insertedEmails
            ],
            201
        );
    }

    /**
     * @param Request $request
     */
    public function showAllEmails(Request $request)
    {
        dd('Not implemented yet');
    }

    /**
     * @param int $offset
     * @param int $limit
     * @return \Illuminate\Http\JsonResponse
     */
    public function showPaginated(int $offset = 0, int $limit = 10)
    {
        try {
            //It's not enough. Also by using an ORM we wouldn't need to worry about escaping.
            $results = DB::select('select e.id, e.address, e.client_identifier as identifier from email e limit :offset,:limit',
                [
                    'offset' => filter_var($offset, FILTER_SANITIZE_NUMBER_INT),
                    'limit' => filter_var($limit, FILTER_SANITIZE_NUMBER_INT)
                ]
            );

            $emails = [];
            foreach ($results as $result) {
                $emails[$result->id] = [
                    'clientIdentifier' => $result->identifier,
                    'address' => $result->address
                ];
            }
        } catch (\Exception $ex) {
            dd($ex->getMessage());
            return response()->json([
                'success' => false,
                'message' => 'Internal server error'
            ], 500);
        }

        return response()->json(
            [
                'success' => true,
                'message' => $emails
            ],
            200
        );
    }
}
