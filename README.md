Installation instructions

Clone the repo:

    git clone https://bitbucket.org/cosmoteco/test-ghost.git

Setup environment:

    docker-compose up -d --build

Run migrations:

    winpty docker-compose exec app php sample/artisan migrate

Access endpoints:

        - paginated emails: GET http://localhost:8080/api/emails/offset/0/limit/5

        - add emails POST http://localhost:8080/api/emails/
            POST payloadexample:
   
        {"emails": [
               "email": "user@example.com"
                "client": "Test1"
             },
            {
                "email": "mike@hotmail.ninja"
               "client": "test2"
            }
        ]
    }

About:

    Api enpoints to save and retrieve emails.

Technologies:

- Docker for deployment
- Lumen framework
- Mysql
- Nginx
- Composer as a package manager
